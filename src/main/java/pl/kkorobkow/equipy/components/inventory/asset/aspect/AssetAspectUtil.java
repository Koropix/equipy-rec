package pl.kkorobkow.equipy.components.inventory.asset.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
class AssetAspectUtil {
    @Pointcut("execution(* pl.kkorobkow.equipy.components.inventory.asset.AssetResource.*(..))")
    void allAssetResourceMethods() {}
}
