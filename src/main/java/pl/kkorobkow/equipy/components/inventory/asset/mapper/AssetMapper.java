package pl.kkorobkow.equipy.components.inventory.asset.mapper;

import org.springframework.stereotype.Service;
import pl.kkorobkow.equipy.components.inventory.asset.Asset;
import pl.kkorobkow.equipy.components.inventory.asset.dto.AssetDto;
import pl.kkorobkow.equipy.components.inventory.category.Category;
import pl.kkorobkow.equipy.components.inventory.category.CategoryRepository;

import java.util.Optional;

@Service
public class AssetMapper {
    private final CategoryRepository categoryRepository;

    public AssetMapper(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public AssetDto toDto(Asset asset) {
        AssetDto dto = new AssetDto();
        dto.setId(asset.getId());
        dto.setName(asset.getName());
        dto.setDescription(asset.getDescription());
        dto.setSerialNumber(asset.getSerialNumber());
        if (asset.getCategory() != null)
            dto.setCategory(asset.getCategory().getName());
        return dto;
    }

    public Asset toEntity(AssetDto dto) {
        Asset asset = new Asset();
        asset.setId(dto.getId());
        asset.setName(dto.getName());
        asset.setDescription(dto.getDescription());
        asset.setSerialNumber(dto.getSerialNumber());
        Optional<Category> category = categoryRepository.findByName(dto.getCategory());
        category.ifPresent(asset::setCategory);
        return asset;
    }
}
