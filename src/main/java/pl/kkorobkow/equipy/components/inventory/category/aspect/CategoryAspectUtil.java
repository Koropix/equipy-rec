package pl.kkorobkow.equipy.components.inventory.category.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
class CategoryAspectUtil {
    @Pointcut("execution(* pl.kkorobkow.equipy.components.inventory.category.CategoryResource.*(..))")
    void allCategoryResourceMethods() {}
}
