package pl.kkorobkow.equipy.components.inventory.category.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import pl.kkorobkow.equipy.common.GeneralLoggingAspect;
import pl.kkorobkow.equipy.components.inventory.category.CategoryResource;

@Aspect
@Component
class CategoryLoggingAspect {
    private final GeneralLoggingAspect generalLoggingAspect;
    private final Logger logger;

    CategoryLoggingAspect(GeneralLoggingAspect generalLoggingAspect) {
        this.generalLoggingAspect = generalLoggingAspect;
        logger = LoggerFactory.getLogger(CategoryResource.class);
    }

    @Before("CategoryAspectUtil.allCategoryResourceMethods()")
    void logInfoBefore(JoinPoint joinPoint) {
        generalLoggingAspect.logInfoBefore(joinPoint, logger);
    }

    @AfterThrowing(pointcut = "CategoryAspectUtil.allCategoryResourceMethods()", throwing = "error")
    void logError(JoinPoint joinPoint, Throwable error) {
        generalLoggingAspect.logError(joinPoint, logger, error);
    }

    @AfterReturning(pointcut = "CategoryAspectUtil.allCategoryResourceMethods()", returning = "result")
    void logSuccess(JoinPoint joinPoint, Object result) {
        generalLoggingAspect.logSuccess(joinPoint, logger, result);
    }

}
