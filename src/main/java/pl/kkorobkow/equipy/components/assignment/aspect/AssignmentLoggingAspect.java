package pl.kkorobkow.equipy.components.assignment.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import pl.kkorobkow.equipy.common.GeneralLoggingAspect;
import pl.kkorobkow.equipy.components.assignment.AssignmentResource;

@Aspect
@Component
class AssignmentLoggingAspect {
    private final GeneralLoggingAspect generalLoggingAspect;
    private final Logger logger;

    AssignmentLoggingAspect(GeneralLoggingAspect generalLoggingAspect) {
        this.generalLoggingAspect = generalLoggingAspect;
        logger = LoggerFactory.getLogger(AssignmentResource.class);
    }

    @Before("AssignmentAspectUtil.allAssignmentResourceMethods()")
    void logInfoBefore(JoinPoint joinPoint) {
        generalLoggingAspect.logInfoBefore(joinPoint, logger);
    }

    @AfterThrowing(pointcut = "AssignmentAspectUtil.allAssignmentResourceMethods()", throwing = "error")
    void logError(JoinPoint joinPoint, Throwable error) {
        generalLoggingAspect.logError(joinPoint, logger, error);
    }

    @AfterReturning(pointcut = "AssignmentAspectUtil.allAssignmentResourceMethods()", returning = "result")
    void logSuccess(JoinPoint joinPoint, Object result) {
        generalLoggingAspect.logSuccess(joinPoint, logger, result);
    }
}
