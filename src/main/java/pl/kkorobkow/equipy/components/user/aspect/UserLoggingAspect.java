package pl.kkorobkow.equipy.components.user.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import pl.kkorobkow.equipy.common.GeneralLoggingAspect;
import pl.kkorobkow.equipy.components.user.UserResource;

@Aspect
@Component
class UserLoggingAspect {
    private final GeneralLoggingAspect generalLoggingAspect;
    private final Logger logger;

    UserLoggingAspect(GeneralLoggingAspect generalLoggingAspect) {
        this.generalLoggingAspect = generalLoggingAspect;
        logger = LoggerFactory.getLogger(UserResource.class);
    }

    @Before("UserAspectUtil.allUserResourceMethods()")
    void logInfoBefore(JoinPoint joinPoint) {
        generalLoggingAspect.logInfoBefore(joinPoint, logger);
    }

    @AfterThrowing(pointcut = "UserAspectUtil.allUserResourceMethods()", throwing = "error")
    void logError(JoinPoint joinPoint, Throwable error) {
        generalLoggingAspect.logError(joinPoint, logger, error);
    }

    @AfterReturning(pointcut = "UserAspectUtil.allUserResourceMethods()", returning = "result")
    void logSuccess(JoinPoint joinPoint, Object result) {
        generalLoggingAspect.logSuccess(joinPoint, logger, result);
    }

}
