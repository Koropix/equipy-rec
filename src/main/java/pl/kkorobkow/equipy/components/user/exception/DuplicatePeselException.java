package pl.kkorobkow.equipy.components.user.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Inny użytkownik z takim peselem już istnieje")
public class DuplicatePeselException extends RuntimeException {
}
