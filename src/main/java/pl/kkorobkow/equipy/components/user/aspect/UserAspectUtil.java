package pl.kkorobkow.equipy.components.user.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
class UserAspectUtil {
    @Pointcut("execution(* pl.kkorobkow.equipy.components.user.UserResource.*(..))")
    void allUserResourceMethods() {}
}
