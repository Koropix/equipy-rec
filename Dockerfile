FROM openjdk:14
ADD build/libs/equipy-0.0.1-SNAPSHOT.jar .
EXPOSE 8000
CMD java -jar equipy-0.0.1-SNAPSHOT.jar --spring.config.import=$CONFIG_LOCATION